<?php
if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
if (CModule::IncludeModule("aero.academy")) {
    $arResult['STUDENTS'] = aero\academy\Academy::getStudents();
}
$this->IncludeComponentTemplate();
