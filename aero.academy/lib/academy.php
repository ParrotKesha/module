<?php

namespace aero\academy;

use CIBlockElement;
use CModule;

class Academy
{

    public static function getStudents() {
        $arOrder = Array("SORT"=>"ASC");
        $arFilter = Array("IBLOCK_ID"=>4, "IBLOCK_SECTION_ID"=>16, "ACTIVE_DATE"=>"Y", "ACTIVE"=>"Y");
        $arSelect = Array("ID", "IBLOCK_ID", "FIRSTNAME", "LASTNAME");
        CModule::IncludeModule("iblock");
        $res = CIBlockElement::getList($arOrder, $arFilter, false, false, $arSelect);
        $result = array();
        while($ob = $res->GetNextElement()) {
            $arProps = $ob->GetProperties();
            $str = $arProps["LASTNAME"]["VALUE"]." ".$arProps["FIRSTNAME"]["VALUE"];
            array_push($result, $str);
        }
        return $result;
    }
}
